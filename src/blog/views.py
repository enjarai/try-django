# from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render, get_object_or_404, redirect
from .forms import BlogPostModelForm, BlogCommentModelForm
from .models import BlogPost, BlogComment
from django.utils import timezone
# Create your views here.


def blog_post_list_view(request):
    now = timezone.now()

    qs = BlogPost.objects.published()
    if request.user.is_staff:
        qs = BlogPost.objects.all()

    template_name   = 'blog/post/list.html'
    context         = {'post_list': qs, 'now': now}

    return render(request, template_name, context)


@staff_member_required
def blog_post_create_view(request):
    form = BlogPostModelForm(request.POST or None, request.FILES or None) # throw post data (if any) into a form object
    valid = form.is_valid()
    if valid: # let django check the form
        post = form.save(commit=False) # get data from form
        post.user = request.user # add author to post
        post.save() # save post into the database
        form = BlogPostModelForm() # empty the form when done

    template_name   = 'blog/post/create.html'
    context         = {
        'title': 'Create Post',
        'form': form,
        'valid': valid,
    }

    return render(request, template_name, context)


def blog_post_detail_view(request, post_slug):
    now = timezone.now()
    post = get_object_or_404(BlogPost, slug=post_slug)
    comments = BlogComment.objects.filter(post=post)

    form = BlogCommentModelForm(request.POST or None)
    valid = form.is_valid()
    if valid:
        comment = form.save(commit=False)
        comment.user = request.user
        comment.post = post
        comment.save()
        form = BlogCommentModelForm()

    can_edit = request.user.is_staff
    can_comment = request.user.is_authenticated

    template_name   = 'blog/post/detail.html'
    context         = {
        'post': post,
        'comments': comments,
        'form': form,
        'can_edit': can_edit,
        'can_comment': can_comment,
        'now': now,
        'valid': valid,
    }

    return render(request, template_name, context)


@staff_member_required
def blog_post_update_view(request, post_slug):
    post = get_object_or_404(BlogPost, slug=post_slug)

    form = BlogPostModelForm(request.POST or None, request.FILES or None, instance=post)
    valid = form.is_valid()
    if valid:
        form.save()

    template_name   = 'blog/post/update.html'
    context         = {
        'title': f'Update {post.title}',
        'form': form,
        'valid': valid,
    }

    return render(request, template_name, context)


@staff_member_required
def blog_post_delete_view(request, post_slug):
    post = get_object_or_404(BlogPost, slug=post_slug)

    if request.method == "POST":
        post.delete()
        return redirect("../../")

    template_name   = 'blog/post/delete.html'
    context         = {'post': post, 'form': None}

    return render(request, template_name, context)
