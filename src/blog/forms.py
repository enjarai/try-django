from django import forms

from .models import (
    BlogPost,
    BlogComment,
)


#class BlogPostForm(forms.Form):
#    title   = forms.CharField()
#    slug    = forms.SlugField()
#    content = forms.CharField(widget=forms.Textarea)

class BlogPostModelForm(forms.ModelForm):
    class Meta:
        model   = BlogPost
        fields  = [
            'title',
            'image',
            'slug',
            'content',
            'publish_date',
        ]

class BlogCommentModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(forms.ModelForm, self).__init__(*args, **kwargs)
        self.fields['content'].label = ''

    class Meta:
        model   = BlogComment
        fields  = [
            'content',
        ]
