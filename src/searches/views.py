from django.shortcuts import render

from blog.models import BlogPost

from .models import SearchQuery

# Create your views here.
def search_view(request):
    query = request.GET.get('q', None)

    user = None
    if request.user.is_authenticated:
        user = request.user

    qs = None
    if query is not None:
        SearchQuery.objects.create(user=user, query=query)
        qs = BlogPost.objects.search(query=query)

    context = {'query': query, 'results': qs}
    return render(request, 'searches/view.html', context)
